Prepare environment:

    module purge
    module load Python/3.11.5
    python3 -m venv .venv
    source .venv/bin/activate
    python3 -m pip install -r requirements.txt
    pip install cookicutter
    profile_dir="${HOME}/.config/snakemake"
    mkdir -p "$profile_dir"
    template="gh:Snakemake-Profiles/slurm"
    cookiecutter --output-dir "$profile_dir" "$template"
    # non-default choices:
    # jobs (500): 144
    # restart_times (0): 3
    # latency_wait (5): 240
    # sbatch_defaults (): qos=hubbioit partition=hubbioit
    # cluster_logpath (logs/slurm/%r/%j): logs/slurm/%r/%x
    cp -r ~/.config/snakemake/slurm/ src/profile/.
    # Then edit src/profile/config.yaml according to https://github.com/Snakemake-Profiles/slurm/issues/117#issuecomment-1906448548


Use environment to run the workflow:

    module purge
    module load Python/3.11.5
    source .venv/bin/activate
    VENV=".venv" ./src/run_workflow.sh src/jointcall_config.yaml --rerun-incomplete
    VENV=".venv" ./src/run_workflow.sh src/jointcall_start_and_end_points_config.yaml --rerun-incomplete
    VENV=".venv" ./src/run_workflow.sh src/jointcall_all_samples_config.yaml --rerun-incomplete
