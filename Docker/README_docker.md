This directory contains a Dockerfile to build an image containing R and the
necessary packages to run the loci filtering script `filter_variants.R`, as
well as this script (installed in the PATH).

To build the image for tests (increment version number as needed):

    cp -f ../src/R_scripts/filter_variants.R .
    docker build -t renvir:0.1 .

To test it (mounting a local directory in the container):

    docker run -v /data/19504_Smith/jointcall_results:/mnt -it renvir:0.1

This will have to be registered in the docker registry of the pasteur gitlab
(see <https://gitlab.pasteur.fr/hub/19504_smith/container_registry>):

    docker login registry-gitlab.pasteur.fr
    # Build and push a tagged version
    cp -f ../src/R_scripts/filter_variants.R .
    docker build -t registry-gitlab.pasteur.fr/hub/19504_smith/renvir:0.1 .
    docker push registry-gitlab.pasteur.fr/hub/19504_smith/renvir:0.1


