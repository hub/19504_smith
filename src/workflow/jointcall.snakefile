from hashlib import md5
import os
from pathlib import Path
from pickle import dump, HIGHEST_PROTOCOL
from textwrap import indent

from mappy import fastx_read
import pandas as pd
from libsnp import counts2freqs, load_counts_from_joint_vcf
from libworkflows import run_with_modules

module_versions = {
    "bioawk": "1.0",
    "fastqc": "0.11.9",
    "freebayes": "v1.3.3",
    "snpEff": "4.3",
    "cutadapt": "2.10",
    "bwa": "0.7.17",
    "graalvm": "ce-java19-22.3.1",
    "R": "3.6.2",
    "picard-tools": "2.23.3",
    "samtools": "1.18",
    "java": "1.8.0",
    "bamtools": "2.5.2",
    # http://qualimap.conesalab.org/doc_html/analysis.html#multi-sample-bam-qc
    # http://qualimap.conesalab.org/doc_html/command_line.html#cmdline-multibamqc
    "qualimap": "v2.2.1",
    # Also provided by htslib, possibly newer
    #"tabix": "0.2.6",
    # A USER ERROR has occurred: RealignerTargetCreator is no longer included in GATK as of version 4.0.0.0. Please use GATK3 to run this tool
    # A USER ERROR has occurred: IndelRealigner is no longer included in GATK as of version 4.0.0.0. Please use GATK3 to run this tool
    #"GenomeAnalysisTK": "4.1.9.0"
    "GenomeAnalysisTK": "3.6",
    "htslib": "1.10",
    "vcflib": "1.0.1"
}

qos_dict = {}

def run_and_write_methods(
        cmd, params, output,
        methods_before="", methods_after="",
        verbose=False):
    """
    Run a shell command *cmd*, optionally loading some environment modules.

    If *params* has an attribute "modules" containing a list of environment
    modules, the command loading the modules will be pre-pended to the main
    command *cmd*.

    if *output* has a "methods" attribute, the command *cmd* will be written
    into the corresponding file. *methods_before* and *methods_after* will also
    be written in this file, before and after the command, respectively.
    """
    if hasattr(params, "modules"):
        # TODO (12/02/2024): Use the native mechanism?
        # https://snakemake.readthedocs.io/en/stable/snakefiles/deployment.html#using-environment-modules
        # Check how module loading is logged.
        cmd = run_with_modules(
            cmd,
            modules=[
                f"{module}/{module_versions[module]}"
                for module in params.modules])
    if verbose:
        print(cmd)
    shell(cmd)
    if hasattr(output, "methods"):
        cmd = indent(cmd, "\t")
        with open(output.methods, "w") as fh:
            fh.write(f"{methods_before}\n{cmd}\n{methods_after}\n")

def get_ids(name):
    """Extract identifiers from fastq name *name*."""
    [instrument, run_id, flowcell, lane, *_] = name.split(":")
    return (instrument, run_id, flowcell, lane)

def make_read_group(sample, fq):
    """
    Build a @RG sam header to help downstream processing.
    """
    (name, _, _, comment) = next(fastx_read(fq, read_comment=True))
    (instrument, run_id, flowcell, lane) = get_ids(name)
    index_seq = comment.split(":")[-1]
    rg_id = f"ID:{flowcell}.{lane}.{sample}"
    sample_id = f"SM:{sample}"
    PU = f"PU:{flowcell}.{lane}.{index_seq}"
    PL = "PL:Illumina"
    # change "lib1" if library preparation was re-done at the wetlab level
    LB = f"LB:{sample}.lib1"
    readgroup=f"@RG\\t{rg_id}\\t{sample_id}\\t{PU}\\t{PL}\\t{LB}"
    return readgroup


# Used to increase memory requirements
# when a rule instance has to be retried
def get_mem_mb(wildcards, attempt, threads):
    #return (90 ** attempt) // 2
    return 4096 if attempt == 1 else (10240 if attempt == 2 else (480000 if attempt == 3 else 1980000))



genome = config["genome"]["name"]
outdir = Path(config.get("outdir", "."))
outdir.mkdir(exist_ok=True, parents=True)
chr_sizes = {
    name: len(seq)
    for (name, seq, *_) in fastx_read(config["genome"]["fasta"])}

chromosomes = list(chr_sizes.keys())

class Chromosome():
    """
    Class helping the computation of chromosome parts.
    """
    def __init__(self, name, length, part_len):
        self.name = name
        self.length = length
        self.parts_dict = self.set_parts(part_len)
        self.parts = [part_num for part_num in self.parts_dict.keys()]

    def set_parts(self, part_len):
        parts_dict = {}
        part_num = 1
        start = 0
        end = part_len
        while end < self.length:
            parts_dict[str(part_num)] = (start, end)
            start = end
            end = end + part_len
            part_num += 1
        parts_dict[str(part_num)] = (start, self.length)
        return parts_dict

# SNP calling will be done in parallel
# on chromosome portions of this length
#part_len = 100000
part_len = config.get("part_len", 50000)

chrom_dict = {
    chrom_name: Chromosome(chrom_name, chrom_size, part_len)
    for (chrom_name, chrom_size) in chr_sizes.items()}

sample_table = pd.read_table(
    config["sample_table"],
    index_col=0, sep="\t")
samples = tuple(sorted(sample_table.index.values))
#print(", ".join(samples))

# md5sum representing the list of samples
# for which the SNP calling was done jointly
m = md5()
for sample in samples:
    m.update(sample.encode())

md5hash = m.hexdigest()
md52samples = {md5hash: samples}
samples2md5 = {samples: md5hash}

#/pasteur/zeus/projets/p02/Project_19504/WT1_SK_timecourse/SK_WT_P0/P0_WT1_EKDN220047611-1A_HKHYCDSX5_L2_1.fq.gz_R2_raw.fq.gz
link_raw_out = [
    outdir.joinpath("data", "raw", f"{sample}_{mate}_raw.fq.gz")
    for mate in ["R1", "R2"]
    for sample in samples]

fastqc_out = [
    outdir.joinpath("data", "fastqc", sample, f"{sample}_{mate}_{maturity}_fastqc.html")
    for maturity in ["raw", "clean"]
    for mate in ["R1", "R2"]
    for sample in samples]

#cutadapt_out = [
#    outdir.joinpath("data", "clean", f"{sample}_{mate}_clean.fq.gz")
#    for mate in ["R1", "R2"]
#    for sample in samples]

qualimap_outdirs = [
    outdir.joinpath(f"mapped_on_{genome}", maturity, "qualimap", sample)
    for sample in samples
    for maturity in ["clean", "dedup", "proper_inserts", "realigned"]]

by_chrom = True
by_part = True

chrom_parts = [
    outdir.joinpath("chromosome_parts.bed")]

final_vcfs = [
    outdir.joinpath(f"mapped_on_{genome}", "realigned", f"FB_{md5hash}", "FB.vcf.gz"),
    outdir.joinpath(f"mapped_on_{genome}", "realigned", f"FB_{md5hash}", "FB.vcf.gz.tbi")]

annotated_vcfs = []
for final_vcf in final_vcfs:
    if final_vcf.suffix == ".gz":
        assert Path(final_vcf.stem).suffix == ".vcf"
        annotated_vcf = final_vcf.parent.joinpath(
            f"{Path(final_vcf.stem).stem}.snpEFF.vcf.gz")
        annotated_vcf_index = final_vcf.parent.joinpath(
            f"{Path(final_vcf.stem).stem}.snpEFF.vcf.gz.tbi")
        annotated_vcfs.extend([annotated_vcf, annotated_vcf_index])

snp_tables = [
    outdir.joinpath(f"mapped_on_{genome}", "realigned", f"snp_tables_{md5hash}", f"{sample}_snps.tsv")
    for sample in md52samples[md5hash]]

wildcard_constraints:
    md5hash="\w+"

rule all:
    input:
        link_raw_out,
        fastqc_out,
        # TODO (20/02/2024): re-activate qualimap
        qualimap_outdirs,
        #cutadapt_out,
        chrom_parts,
        final_vcfs,
        annotated_vcfs,
        snp_tables,
        outdir.joinpath(f"mapped_on_{genome}", "realigned", f"FB_{md5hash}", "snp_selection.tsv"),

rule store_chrom_parts:
    output:
        bed = outdir.joinpath("chromosome_parts.bed")
    run:
        with open(output.bed, "w") as fh:
            for chrom_name in chromosomes:
                chromosome = chrom_dict[chrom_name]
                for part in chromosome.parts:
                    (start, end) = chromosome.parts_dict[part]
                    fh.write(f"{chrom_name}\t{start}\t{end}\t{chrom_name}_{part}\n")

rule store_hashdicts:
    output:
        hashfile=outdir.joinpath("md5hashes.pickle")
    run:
        with open(hashfile, "wb") as fh:
            dump(md52samples, fh, HIGHEST_PROTOCOL)
            dump(samples2md5, fh, HIGHEST_PROTOCOL)


#rule make_chr_list:
#    output:
#        chrlist=outdir.joinpath("chromosomes.list")
#    run:
#        chrfile = genome_dir.joinpath("genome.chrSize")
#        shell(f"cut -f 1 {chrfile} > {output.chrlist}")


def lib2data(wildcards):
    # Should be a dict {"fq1": <path_to_R1>, "fq2": <path_to_R2>}
    return sample_table[["fq1", "fq2"]].loc[wildcards.sample].str.split(",").to_dict()
    #return sample_table[["fq1", "fq2"]].loc[wildcards.sample].to_dict()


rule link_raw_data:
    """This rule installs the raw data in a local directory using symlinks.
    The location of the original files is taken from the configuration.
    If the data for one mate consists in several runs, the result will be
    a concatenation instead of a link.
    """
    input:
        unpack(lib2data),
    output:
        link_fq1 = outdir.joinpath("data", "raw", "{sample}_R1_raw.fq.gz"),
        link_fq2 = outdir.joinpath("data", "raw", "{sample}_R2_raw.fq.gz"),
    message:
        "Making links {output} to raw data {input}."
    group:
        "pre_map_{sample}"
    resources:
        # To be able to use qos=fast in grouped mode (together with do_cutadapt)
        runtime = 10
    run:
        outdir.joinpath("data", "raw").mkdir(parents=True, exist_ok=True)
        if len(input.fq1) > 1:
            to_cat = " ".join([f"'{fq}'" for fq in input.fq1])
            cmd = f"cat {to_cat} > {output.link_fq1}"
            print(cmd)
            shell(cmd)
        else:
            os.symlink(os.path.abspath(input.fq1[0]), output.link_fq1)
        if len(input.fq2) > 1:
            to_cat = " ".join([f"'{fq}'" for fq in input.fq2])
            cmd = f"cat {to_cat} > {output.link_fq2}"
            print(cmd)
            shell(cmd)
        else:
            os.symlink(os.path.abspath(input.fq2[0]), output.link_fq2)


def source_fastq(wildcards):
    if not hasattr(wildcards, "maturity"):
        wildcards.maturity = "raw"
    return {
        "fq1": outdir.joinpath("data", wildcards.maturity, f"{wildcards.sample}_R1_{wildcards.maturity}.fq.gz"),
        "fq2": outdir.joinpath("data", wildcards.maturity, f"{wildcards.sample}_R2_{wildcards.maturity}.fq.gz"),
        }


rule do_fastqc:
    input:
        # Could be done directly instead of using an input function
        unpack(source_fastq),
    output:
        fastqc_R1 = outdir.joinpath("data", "fastqc", "{sample}", "{sample}_R1_{maturity}_fastqc.html"),
        fastqc_R2 = outdir.joinpath("data", "fastqc", "{sample}", "{sample}_R2_{maturity}_fastqc.html"),
        methods = outdir.joinpath("methods", "fastqc", "{sample}_{maturity}_fastqc_methods.md")
    params:
        fastqc_outdir = lambda wildcards: outdir.joinpath("data", "fastqc", wildcards.sample),
        modules = ["graalvm", "fastqc"]
    threads: 2
    resources:
        mem_mb = 2048
    run:
        cmd = f"""
mkdir -p {params.fastqc_outdir}
fastqc -t {threads} -o {params.fastqc_outdir} {input.fq1} {input.fq2}
"""
        run_and_write_methods(cmd, params, output)


rule do_cutadapt:
    input:
        unpack(source_fastq),
    output:
        fq1 = outdir.joinpath("data", "clean", "{sample}_R1_clean.fq.gz"),
        fq2 = outdir.joinpath("data", "clean", "{sample}_R2_clean.fq.gz"),
        methods = outdir.joinpath("methods", "cutadapt", "{sample}_cutadapt_methods.md")
    params:
        cutadapt_outdir = lambda wildcards: outdir.joinpath("data", "clean"),
        modules = ["cutadapt"]
    group:
        "pre_map_{sample}"
    resources:
        # To be able to use qos=fast in grouped mode (together with lin_raw_data)
        runtime = 110
    threads: 2
    run:
        cmd = f"""
mkdir -p {params.cutadapt_outdir}
cutadapt -j {threads} --trim-n -q 30 --minimum-length=20 -o {output.fq1} -p {output.fq2} {input.fq1} {input.fq2}
"""
        run_and_write_methods(cmd, params, output)


rule bwa_index:
    input:
        fa = config["genome"]["fasta"]
    output:
        #idx = multiext(outdir.joinpath("data", "genome", f"{genome}"), ".amb", ".ann", ".bwt", ".pac", ".sa")
        idx = [
            outdir.joinpath("data", "genome", f"{genome}.{ext}")
            for ext in ["amb", "ann", "bwt", "pac", "sa"]]
    params:
        modules = ["bwa"]
    run:
        prefix = outdir.joinpath("data", "genome", f"{genome}")
        cmd = f"bwa index -p '{prefix}' {input.fa}"
        run_and_write_methods(cmd, params, output)


rule map_bwa:
    input:
        unpack(source_fastq),
        #idx = multiext(outdir.joinpath("data", "genome", f"{genome}"), ".amb", ".ann", ".bwt", ".pac", ".sa"),
        idx = [
            outdir.joinpath("data", "genome", f"{genome}.{ext}")
            for ext in ["amb", "ann", "bwt", "pac", "sa"]]
    output:
        # TODO: make temp()
        bam = outdir.joinpath(f"mapped_on_{genome}", "{maturity}", "{sample}_{maturity}.bam"),
        bai = outdir.joinpath(f"mapped_on_{genome}", "{maturity}", "{sample}_{maturity}.bam.bai"),
        methods = outdir.joinpath("methods", f"bwa_on_{genome}", "{sample}_{maturity}_bwa_methods.md")
    benchmark:
        outdir.joinpath("benchmarks", f"bwa_on_{genome}", "{sample}_{maturity}_bwa_benchmark.txt")
    wildcard_constraints:
        # We still need this wildcard: it is used by the input function
        maturity = "clean"
    params:
        modules = ["bwa", "samtools"]
    threads: 16
    resources:
        runtime = 10,
        mem_mb = 10240
    run:
        prefix = outdir.joinpath("data", "genome", f"{genome}")
        tmpdir_cmd = "tmpdir=$(mktemp -dt 'samtools.XXXXXXXXXX')"
        read_group = make_read_group(wildcards.sample, input.fq1)
        assert read_group == make_read_group(wildcards.sample, input.fq2)
        map_cmd = f"bwa mem -t {threads//2} -M -R '{read_group}' {prefix} {input.fq1} {input.fq2}"
        # -u keeps bam uncompressed to speed up subsequent sorting
        # -F 4 removes reads that have not mapped
        # -q 30 discard unreliably mapped reads
        filter_cmd = "samtools view -u -F 4 -q 30 -"
        sort_cmd = f"samtools sort --threads {threads//2} -T ${{{{tmpdir}}}} -o {output.bam} -"
        index_cmd = f"samtools index {output.bam}"
        clean_cmd = "rm -rf ${{tmpdir}}"
        cmd = f"{tmpdir_cmd}\n{map_cmd} | {filter_cmd} | {sort_cmd}\n{index_cmd}\n{clean_cmd}"
        run_and_write_methods(cmd, params, output, verbose=True)


# https://www.seqanswers.com/forum/bioinformatics/bioinformatics-aa/5774-removing-duplicates-is-it-really-necessary?p=119822#post119822
# > So in my opinion, it would be better to align, realign indels, and then mark duplicates.
# Also, wouldn't it be better to do the indel realignment on merged bam files rather than separately?
rule remove_duplicates:
    input:
        bam = outdir.joinpath(f"mapped_on_{genome}", "clean", "{sample}_clean.bam"),
    output:
        # TODO: make temp()
        bam = outdir.joinpath(f"mapped_on_{genome}", "dedup", "{sample}_dedup.bam"),
        bai = outdir.joinpath(f"mapped_on_{genome}", "dedup", "{sample}_dedup.bam.bai"),
        metrics = outdir.joinpath(f"mapped_on_{genome}", "dedup", "{sample}_dedup_metrics.txt"),
        methods = outdir.joinpath("methods", "remove_duplicates", "{sample}_dedup_methods.md")
    benchmark:
        outdir.joinpath("benchmarks", "remove_duplicates", "{sample}_dedup_benchmark.txt")
    params:
        modules = ["graalvm", "R", "picard-tools", "samtools"]
    resources:
        runtime = 10,
        mem_mb = 2048
    #threads: 16
    #group:
    #    "post_map_{sample}"
    run:
        # What about --ASSUME_SORT_ORDER and --CREATE_INDEX ?
        dedup_cmd = f"picard MarkDuplicates -I {input.bam} -O {output.bam} --REMOVE_DUPLICATES true -M {output.metrics}"
        index_cmd = f"samtools index {output.bam}"
        cmd = f"{dedup_cmd}\n{index_cmd}"
        run_and_write_methods(cmd, params, output)


rule filter_proper_inserts:
    """
    Filter deduped mapped data based on insert size and proper pair mapping
    (same chrom, opposite and converging direction).
    """
    input:
        bam = outdir.joinpath(f"mapped_on_{genome}", "dedup", "{sample}_dedup.bam"),
    output:
        # TODO: make temp()
        bam = outdir.joinpath(f"mapped_on_{genome}", "proper_inserts", "{sample}_proper_inserts.bam"),
        bai = outdir.joinpath(f"mapped_on_{genome}", "proper_inserts", "{sample}_proper_inserts.bam.bai"),
        methods = outdir.joinpath("methods", "filter_inserts", "{sample}_filter_inserts_methods.md")
    benchmark:
        outdir.joinpath("benchmarks", "filter_inserts", "{sample}_filter_inserts_benchmark.txt")
    params:
        #modules = ["bamtools"]
        modules = ["samtools", "bioawk"]
    #threads: 1
    resources:
        runtime = 5
    #group:
    #    "post_map_{sample}"
    run:
        methods_before = """
Mapped reads were further filtered to keep only those mapped in proper pairs
and defining inserts no longer than 1000 bases.
"""
        # samtools view -h -f2 keeps proper pairs and outputs as sam
        # bioawk then removes inserts that are too long
        filter_cmd = f"samtools view -h -f2 {input.bam} | bioawk -Hc sam '$tlen < 1000 {{{{print}}}}'"
        # samtools view -u -b converts sam back to bam
        sort_cmd = f"samtools view -u -b - | samtools sort -o {output.bam}"
        index_cmd = f"samtools index {output.bam}"
        # This version works when run manually, but has weird effects when run in the worklow:
        # Soame samples seem to get mixed and we get incoherent read groups in the reads
        # with respect to the sam header.
        #filter_cmd = f"bamtools filter -in {input.bam} -insertSize '<1000' -isProperPair true"
        #sort_cmd = f"bamtools sort -out {output.bam}"
        #index_cmd = f"bamtools index -in {output.bam}"
        cmd = f"{filter_cmd} | {sort_cmd}\n{index_cmd}"
        run_and_write_methods(cmd, params, output, methods_before=methods_before)


rule link_genome_fasta:
    """
    This rule installs the reference genome sequence in a local directory using symlinks.
    The location of the original file is taken from the configuration.
    """
    input:
        fa = config["genome"]["fasta"]
    output:
        link_fa = outdir.joinpath("data", "genome", f"{genome}.fa")
    run:
        outdir.joinpath("data", "genome").mkdir(parents=True, exist_ok=True)
        os.symlink(os.path.abspath(input.fa), output.link_fa)


rule create_fasta_index_and_dict:
    input:
        fa = rules.link_genome_fasta.output.link_fa,
    output:
        faidx = outdir.joinpath("data", "genome", f"{genome}.fa.fai"),
        fa_dict = outdir.joinpath("data", "genome", f"{genome}.dict")
    params:
        modules = ["samtools", "graalvm", "R", "picard-tools"]
    run:
        idx_cmd = f"samtools faidx {input.fa}"
        dict_cmd = f"picard CreateSequenceDictionary -R {input.fa} -O {output.fa_dict}"
        cmd = f"{idx_cmd}\n{dict_cmd}"
        run_and_write_methods(cmd, params, output)


# TODO
rule create_realignment_targets:
    input:
        fa = rules.link_genome_fasta.output.link_fa,
        faidx = rules.create_fasta_index_and_dict.output.faidx,
        fa_dict = rules.create_fasta_index_and_dict.output.fa_dict,
        bam = rules.filter_proper_inserts.output.bam,
    output:
        intervals = temp(outdir.joinpath(f"mapped_on_{genome}", "realigned", "{sample}.intervals")),
        methods = outdir.joinpath("methods", "realign_indels", "{sample}_create_realignment_targets_methods.md")
    benchmark:
        outdir.joinpath("benchmarks", "create_realignment_targets", "{sample}_create_realignment_targets_benchmark.txt")
    params:
        modules = ["java", "GenomeAnalysisTK"]
    threads: 16
    #group:
    #    "post_map_{sample}"
    # TODO: Base mem_mb on the number of threads ?
    resources:
        runtime = 2,
        #mem_mb = get_mem_mb
        #slurm_extra="--qos=hubbioit"
    run:
        # If splitting by region: -L chrom:start:stop
        cmd = f"GenomeAnalysisTK -T RealignerTargetCreator -R {input.fa} -I {input.bam} -o {output.intervals} -nt {threads}"
        run_and_write_methods(cmd, params, output)

# TODO (01/02/2024): Split by regions if too slow.
# And maybe perform on merged bams for extra accuracy?
rule realign_indels:
    input:
        intervals = rules.create_realignment_targets.output.intervals,
        fa = rules.link_genome_fasta.output.link_fa,
        faidx = rules.create_fasta_index_and_dict.output.faidx,
        fa_dict = rules.create_fasta_index_and_dict.output.fa_dict,
        bam = rules.filter_proper_inserts.output.bam,
    output:
        bam = outdir.joinpath(f"mapped_on_{genome}", "realigned", "{sample}_realigned.bam"),
        bai = outdir.joinpath(f"mapped_on_{genome}", "realigned", "{sample}_realigned.bam.bai"),
        methods = outdir.joinpath("methods", "realign_indels", "{sample}_realigned_methods.md")
    benchmark:
        outdir.joinpath("benchmarks", "realign_indels", "{sample}_realigned_benchmark.txt")
    params:
        modules = ["java", "GenomeAnalysisTK", "samtools"]
    #threads: 16
    #group:
    #    "post_map_{sample}"
    resources:
        runtime = 5,
        mem_mb = get_mem_mb
        #slurm_extra="--qos=hubbioit"
    run:
        # If splitting by region: -L chrom:start:stop
        #create_targets_cmd = f"GenomeAnalysisTK -T RealignerTargetCreator -R {input.fa} -I {input.bam} -o {output.intervals} -nt {threads}"
        # What about -nt {threads} here ?
        # -> likely useless
        # See https://discussions4562.rssing.com/chan-67237868/article3536.html
        realign_cmd = f"GenomeAnalysisTK -T IndelRealigner --targetIntervals {input.intervals} -R {input.fa} -I {input.bam} -o {output.bam}"
        #realign_cmd = f"GenomeAnalysisTK -T IndelRealigner --targetIntervals {output.intervals} -R {input.fa} -I {input.bam} -o {output.bam}"
        index_cmd = f"samtools index {output.bam}"
        #cmd = f"{create_targets_cmd}\n{realign_cmd}\n{index_cmd}"
        cmd = f"{realign_cmd}\n{index_cmd}"
        run_and_write_methods(cmd, params, output)

#qos_dict["realign_indels"] = "hubbioit"

def source_bams(wildcards):
    # "Patching" wildcards, assuming we want the "final" type of bam
    if not hasattr(wildcards, "maturity"):
        wildcards.maturity = "realigned"
    # /pasteur/zeus/projets/p02/Project_19504/Mutant_SK_timecourse/SK_D3_P85/SK_Delta3_EKDN220047637-1A_HKHYCDSX5_L2_1.q.gz_clean.bam
    return {
        sample: outdir.joinpath(f"mapped_on_{genome}", f"{wildcards.maturity}", f"{sample}_{wildcards.maturity}.bam")
        for sample in md52samples[wildcards.md5hash]}


rule make_bam_list:
    """Intended of use as input of qualimap multi-bamqc"""
    input:
        unpack(source_bams),
    output:
        bam_list = temp(outdir.joinpath(f"mapped_on_{genome}", "{maturity}", "{maturity}_{md5hash}_bams.tsv")),
    resources:
        runtime = 5,
        mem_mb = 1024
    run:
        with open(output.bam_list, "w") as fh:
            # Not sure input can be handled as dict
            fh.write("\n".join([
                f"{sample}\t{path}"
                for (sample, path)
                in input.items()]))

# TODO: run on individual samples
#rule qualimap:
#    input:
#        bam_list = outdir.joinpath(f"mapped_on_{genome}", "{maturity}", "{maturity}_{md5hash}_bams.tsv"),
#    output:
#        qualimap_outdir = directory(outdir.joinpath(f"mapped_on_{genome}", "{maturity}", "qualimap_{md5hash}")),
#        methods = outdir.joinpath("methods", "qualimap_{md5hash}", "{maturity}_qualimap_methods.md")
#    benchmark:
#        outdir.joinpath("benchmarks", "qualimap_{md5hash}", "{maturity}_qualimap_benchmark.txt")
#    params:
#        modules = ["graalvm", "R", "qualimap"]
#    resources:
#        runtime = lambda wildcards: len(md52samples[wildcards.md5hash])
#    run:
#        cmd = f"qualimap multi-bamqc -d {input.bam_list} -outdir {output.qualimap_outdir} -r"
#        run_and_write_methods(cmd, params, output)

rule qualimap:
    input:
        bam = outdir.joinpath(f"mapped_on_{genome}", "{maturity}", "{sample}_{maturity}.bam"),
    output:
        qualimap_outdir = directory(outdir.joinpath(f"mapped_on_{genome}", "{maturity}", "qualimap", "{sample}")),
        methods = outdir.joinpath("methods", "qualimap", "{maturity}_{sample}_qualimap_methods.md")
    benchmark:
        outdir.joinpath("benchmarks", "qualimap", "{maturity}_{sample}_qualimap_benchmark.txt")
    params:
        modules = ["graalvm", "R", "qualimap"]
    resources:
        runtime = 10,
        mem_mb = 1024
    run:
        cmd = f"qualimap bamqc -bam {input.bam} -outdir {output.qualimap_outdir}"
        run_and_write_methods(cmd, params, output)

# -G 75: minimum 75 reads supporting alt in total across samples (instead of 1, more stringent)
# -F 0.001: minimum 0.001 alt allele freq in at least one sample (instead of 0.05, less stringent)
# -q 20: minimum base quality supporting allele  (instead of 0)
freebayes_opts = "--pooled-continuous -G 75 -F 0.001 -q 20"
freebayes_methods = f"""
SNP calling was performed jointly on the bam files for all the included samples
using freebayes {module_versions["freebayes"]} with the help of a Snakemake workflow
in order to parallellize the process across chromosomes, and across
{part_len} bp portions within each chromosome.
Options `{freebayes_opts}` of freebayes were used.
"""

# https://github.com/freebayes/freebayes?tab=readme-ov-file#calling-variants-in-a-population
rule jointcall_FB_by_chrom_by_part:
    input:
        unpack(source_bams),
    output:
        vcf=temp(outdir.joinpath(f"mapped_on_{genome}", "realigned", "FB_{md5hash}", "by_chrom", "{chrom}", "FB_{chrom}_{part}.vcf.gz")),
        vcf_idx=temp(outdir.joinpath(f"mapped_on_{genome}", "realigned", "FB_{md5hash}", "by_chrom", "{chrom}", "FB_{chrom}_{part}.vcf.gz.tbi")),
        methods = outdir.joinpath("methods", "jointcall_FB_{md5hash}", "{chrom}_{part}_jointcall_methods.md")
    benchmark:
        outdir.joinpath("benchmarks", "jointcall_FB_{md5hash}", "{chrom}_{part}_jointcall_benchmark.txt")
    params:
        modules = ["freebayes", "htslib"],
        fa = config["genome"]["fasta"],
    resources:
        #mem_mb = get_mem_mb,
        #slurm_extra="--qos hubbioit"
    run:
        input_part = " ".join([
            f"--bam {bam}"
            for (_, bam) in input.items()])
        (part_start, part_end) = chrom_dict[wildcards.chrom].parts_dict[wildcards.part]
        region_part = f"--region {wildcards.chrom}:{part_start}-{part_end}"
        #cmd = f"freebayes --pooled-continuous -f {params.fa} {input_part} {region_part} | bgzip > {output.vcf}\ntabix -p vcf -f {output.vcf}\n"
        cmd = f"freebayes {freebayes_opts} -f {params.fa} {input_part} {region_part} | bgzip > {output.vcf}\ntabix -p vcf -f {output.vcf}\n"
        run_and_write_methods(cmd, params, output, methods_before=freebayes_methods)

qos_dict["jointcall_FB_by_chrom_by_part"] = "hubbioit"

def source_chrom_parts(wildcards):
    return [
        outdir.joinpath(f"mapped_on_{genome}", "realigned", f"FB_{wildcards.md5hash}", "by_chrom", wildcards.chrom, f"FB_{wildcards.chrom}_{part}.vcf.gz")
        for part in chrom_dict[wildcards.chrom].parts]

rule merge_vcf_parts_for_chrom:
    input:
        source_chrom_parts,
    output:
        vcf=temp(outdir.joinpath(f"mapped_on_{genome}", "realigned", "FB_{md5hash}", "{chrom}", "FB_{chrom}.vcf.gz")),
        vcf_idx=temp(outdir.joinpath(f"mapped_on_{genome}", "realigned", "FB_{md5hash}", "{chrom}", "FB_{chrom}.vcf.gz.tbi")),
        methods = outdir.joinpath("methods", "jointcall_FB_{md5hash}", "{chrom}_merge_parts_methods.md")
    benchmark:
        outdir.joinpath("benchmarks", "merge_parts_{md5hash}", "{chrom}_merge_parts_benchmark.txt")
    params:
        modules = ["samtools", "htslib"]
        #modules = ["htslib", "vcflib", "tabix"]
    resources:
        runtime = 1
        #mem_mb = get_mem_mb,
        #slurm_extra="--qos hubbioit"
    run:
        # TODO (15/02/2024): test manually
        # -> write-index does not seem to work (https://github.com/samtools/bcftools/issues/2099)
        #cmd = f"bcftools concat --naive -o {output.vcf} -Oz --write-index {input}\ntabix -p vcf -f {output.vcf}\n"
        cmd = f"bcftools concat --naive -o {output.vcf} -Oz {input}\ntabix -p vcf -f {output.vcf}\n"
        #cmd=f"zcat {input} | vcffirstheader | vcfstreamsort -w 1000 | vcfuniq | bgzip > {output.vcf}\ntabix -p vcf -f {output.vcf}\n"
        run_and_write_methods(cmd, params, output)

qos_dict["merge_vcf_parts_for_chrom"] = "hubbioit"

rule merge_vcf:
    input:
        expand(outdir.joinpath(f"mapped_on_{genome}", "realigned", "FB_{{md5hash}}", "{chrom}", "FB_{chrom}.vcf.gz"), chrom=chromosomes),
    output:
        vcf=outdir.joinpath(f"mapped_on_{genome}", "realigned", "FB_{md5hash}", "FB.vcf.gz"),
        vcf_idx=outdir.joinpath(f"mapped_on_{genome}", "realigned", "FB_{md5hash}", "FB.vcf.gz.tbi"),
        methods = outdir.joinpath("methods", "jointcall_FB_{md5hash}", "merge_chroms_methods.md")
    benchmark:
        outdir.joinpath("benchmarks", "merge_chroms_{md5hash}", "merge_chroms_benchmark.txt")
    params:
        modules = ["samtools", "htslib"]
        #modules = ["htslib", "vcflib", "tabix"]
    resources:
        # TODO: make this a function of len(input)
        runtime = 1
        #mem_mb = get_mem_mb,
        #slurm_extra="--qos hubbioit"
    #group:
    #    "post_call"
    run:
        #cmd = f"bcftools concat --naive -o {output.vcf} -Oz --write-index {input}\ntabix -p vcf -f {output.vcf}\n"
        cmd = f"bcftools concat --naive -o {output.vcf} -Oz {input}\ntabix -p vcf -f {output.vcf}\n"
        #cmd=f"zcat {input} | vcffirstheader | vcfstreamsort -w 1000 | vcfuniq | bgzip > {output.vcf}\ntabix -p vcf -f {output.vcf}\n"
        run_and_write_methods(cmd, params, output)

qos_dict["merge_vcf"] = "hubbioit"

rule annotate_with_snpEFF:
    input:
        vcf_in=rules.merge_vcf.output.vcf,
    output:
        vcf=outdir.joinpath(f"mapped_on_{genome}", "realigned", "FB_{md5hash}", "FB.snpEFF.vcf.gz"),
        vcf_idx=outdir.joinpath(f"mapped_on_{genome}", "realigned", "FB_{md5hash}", "FB.snpEFF.vcf.gz.tbi"),
        summary=outdir.joinpath(f"mapped_on_{genome}", "realigned", "FB_{md5hash}", "FB.snpEFF_summary"),
        #vcf="{prefix}.snpEFF.vcf.gz",
        #vcf_idx="{prefix}.snpEFF.vcf.gz.tbi",
        #summary="{prefix}.snpEFF_summary",
        methods=outdir.joinpath("methods", "annotate_with_snpEFF_{md5hash}", "snpEFF_methods.md")
    benchmark:
        outdir.joinpath("benchmarks", "annotate_with_snpEFF_{md5hash}", "snpEFF_benchmark.txt")
    params:
        modules=["graalvm", "snpEff", "htslib"],
        config=config["genome"]["snpEff_config"]
    resources:
        #mem_mb = get_mem_mb,
        mem_mb = 4096,
        #slurm_extra="--qos hubbioit"
    #group:
    #    "post_call"
    run:
        cmd = f"snpEff -ud 0 {genome} {input.vcf_in} -noLog -c {params.config} -stats {output.summary} | bgzip > {output.vcf}\ntabix -p vcf -f {output.vcf}"
        run_and_write_methods(cmd, params, output)

qos_dict["annotate_with_snpEFF"] = "hubbioit"

rule filter_loci:
    input:
        vcf_in=rules.annotate_with_snpEFF.output.vcf,
    output:
        snp_selection=outdir.joinpath(f"mapped_on_{genome}", "realigned", "FB_{md5hash}", "snp_selection.tsv"),
    params:
        filter_settings=config["filter_settings"]
    container:
        # https://snakemake.readthedocs.io/en/stable/snakefiles/deployment.html#running-jobs-in-containers
        "docker://registry-gitlab.pasteur.fr/hub/19504_smith/renvir:0.1"
    shell:
        """
        filter_variants.R {input} {output} {params.filter_settings}
        """
        

# TODO: implement using dynamic rule creation: https://stackoverflow.com/a/78027774/1878788
for md5sum in md52samples:
    rule:
        name: f"extract_snp_tables_{md5sum}"
        input:
            vcf = outdir.joinpath(f"mapped_on_{genome}", "realigned", f"FB_{md5sum}", "FB.snpEFF.vcf.gz"),
        output:
            **{
                sample: outdir.joinpath(f"mapped_on_{genome}", "realigned", f"snp_tables_{md5sum}", f"{sample}_snps.tsv")
                for sample in md52samples[md5sum]}
        resources:
            runtime = 5
        run:
            all_counts = load_counts_from_joint_vcf(
                input.vcf,
                chromosomes,
                biallelic_only=True,
                snp_only=False,
                allow_mnp=True)
            all_freqs = counts2freqs(all_counts)
            for sample in md52samples[md5sum]:
                sample_df = pd.concat(
                    [all_counts[sample], all_freqs[sample]],
                    axis=1)
                sample_df["coverage"] = sample_df[["RO", "AO"]].sum(axis=1)
                sample_df = sample_df.reset_index().set_index("SNP")[[
                    "chrom", "pos", "ref", "alt", "coverage", sample, "gene_id", "EFF_mut"]]
                sample_df.columns = [[
                    "chrom", "pos", "ref", "alt", "coverage", "alt_freq", "gene_id", "EFF_mut"]]
                sample_df.to_csv(getattr(output, sample), sep="\t")
    #qos_dict[f"extract_snp_tables_{md5sum}"] = "hubbioit"

# https://github.com/snakemake/snakemake-executor-plugin-slurm/issues/18#issuecomment-1930377206
for rulename in rules._rules:
    the_rule = getattr(rules, rulename).rule
    qos = qos_dict.get(rulename, "fast")
    runtime = the_rule.resources["runtime"]
    try:
        # Override settings
        if runtime <= 120:
            qos = "fast"
        if runtime <= 5:
            qos = "ultrafast"
    except:
        print(f"Failed to evaluate runtime: {runtime}\nRequesting qos hubbioit")
        qos = "hubbioit"
    # but see https://github.com/snakemake/snakemake-executor-plugin-slurm/issues/33#issuecomment-1936132339
    #slurm_extra = f"--job-name={rulename} --qos={qos}"
    # Partition hubbioit doesn't have access to nodes with more than 480000 MB of RAM.
    #if the_rule.resources["mem_mb"].__call__(wildcards, attempt, threads) > 480000:
    #   File "/pasteur/zeus/projets/p01/BioIT/19504_Smith/src/workflow/jointcall.snakefile", line 1050, in <module>
    # AttributeError: 'int' object has no attribute '__call__'
    # if the_rule.resources["mem_mb"].__call__(wildcards=the_rule.wildcards, attempt=the_rule.attempt, threads=the_rule.threads) > 480000:
    # Likely mostly failing
    try:
        the_rule.resources["mem_mb"].__call__(wildcards=the_rule.wildcards, attempt=the_rule.attempt, threads=the_rule.threads) > 480000
    except AttributeError:
        try:
            high_mem = the_rule.resources["mem_mb"] > 480000
        except TypeError:
            try:
                high_mem = int(the_rule.resources["mem_mb"]) > 480000
            except:
                #print(the_rule.resources["mem_mb"])
                high_mem = False
    if high_mem:
        #try:
        #    # Override settings
        #    if runtime <= 120:
        #        qos = "fast"
        #    if runtime <= 5:
        #        qos = "ultrafast"
        #except:
        #    print(f"Failed to evaluate runtime: {runtime}\nRequesting qos hubbioit")
        #    qos = "hubbioit"
        if qos == "hubbioit":
            qos = "normal"
            if runtime <= 120:
                qos = "fast"
            if runtime <= 5:
                qos = "ultrafast"
            slurm_extra = f"--qos={qos} --partition=common"
    else:
        slurm_extra = f"--qos={qos}"
    # If partition is not set to hubbioit, qos cannot be set to hubbioit:
    # SLURM job submission failed. The error message was sbatch: error: Batch job submission failed: Invalid qos specificatio
    if qos == "hubbioit":
        slurm_extra += " --partition=hubbioit"
    #print(f"setting qos {qos} for rule {the_rule.name}")
    the_rule.resources["slurm_extra"] = slurm_extra
