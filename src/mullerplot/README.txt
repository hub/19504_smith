#extract vcf corresponding to good quality snps (from Pascal R script)
zcat ../../jointcall_results/mapped_on_SKV38_Basile/realigned/FB_7476a6e0dbb539500f0d2ed99a8ade5f/FB.vcf.gz | head -62 | tail -1 > good_quality_snp.vcf
for pos in `cat snp_positions.txt`; do zgrep "SKV38_Basile  $pos    " ../../jointcall_results/mapped_on_SKV38_Basile/realigned/FB_7476a6e0dbb539500f0d2ed99a8ade5f/FB.vcf.gz; done >> good_quality_snp.vcf

#extract good qual snp in Breseq
gawk '{if($1=="RA"){s=split($12,T,"="); split($13,Tfreq,"="); if(T[2]>1e-5 && $0 !~ "polymorphism_reject"){print $5 " " $7 " " $8 " " Tfreq[2] " " T[2]}}}' ../../../BreSeq_results/*/output.gd | cut -d " " -f1 | sort -nu > snp_positions_breseq.txt

#diff in snp lists
diff -y snp_positions.txt snp_positions_breseq.txt

#extract strains list in big vcf
zcat ../../../jointcall_results/mapped_on_SKV38_Basile/realigned/FB_a2634fac5f059556d2a148868e7b70c1/FB.snpEFF.vcf.gz | head -67 | tail -1

#extract a given results in big vcf file 
#(ex: pos 791159 in SK_D3)
zgrep "791159" ../../../jointcall_results/mapped_on_SKV38_Basile/realigned/FB_a2634fac5f059556d2a148868e7b70c1/FB.snpEFF.vcf.gz | cut -f40
