import pandas as pd
#from pymuller import muller

def get_abundance(vcf_def):
    print(vcf_def)
    info=vcf_def.split(":")
    mut=int(info[2].split(",")[1])
    ref=int(info[2].split(",")[0])
    abundance=round(mut/(mut+ref)*100)
    return abundance


df = pd.read_csv('good_quality_snp_without_qual_filter.vcf', sep='\t')

strain="SK_D3_P"
attributes_file = open(strain+"_attributes_without_qual_filter.tsv","w")
population_file = open(strain+"_population_without_qual_filter.tsv","w")
population_table_file = open(strain+"_table_population_without_qual_filter.tsv","w")
#attributes_list_identity = []
#attributes_list_parent = []
#population_list_generation = []
#population_list_identity = []
#population_list_population = []

attributes_file.write("names\tparents\n")
population_file.write("names\ttimes\tabundances\n")

for index, row in df.iterrows():
    print(index)
    print(row[strain+"0"])
    if get_abundance(row[strain+"0"]) > 95:
        continue
    mut=str(row["POS"])+"_"+row["REF"]+"_"+row["ALT"]
    attributes_file.write(mut+"\tNA\n")
    pop_table_line=mut
    #attributes_list_identity.append(mut)
    #attributes_list_parent.append("1499463_T_G")

    #time_point=1
    pop_table_head=""
    for i in range(0,100):
        if strain+str(i) in row:
            population_file.write(mut+"\t"+str(i)+"\t"+str(get_abundance(row[strain+str(i)]))+"\n")
            pop_table_line=pop_table_line+"\t"+str(get_abundance(row[strain+str(i)]))
            pop_table_head=pop_table_head+"\t"+str(i)
            #population_file.write(mut+"\t"+str(time_point)+"\t"+str(get_abundance(row[strain+str(i)]))+"\n")
            #time_point+=1
            #population_list_generation.append(i)
            #population_list_identity.append(mut)
            #population_list_population.append(get_abundance(row[strain+str(i)]))
    if index == 0:
        population_table_file.write(pop_table_head+"\n")        
    population_table_file.write(pop_table_line+"\n")
#populations_df=pd.DataFrame({"Generation" : population_list_generation, "Identity": population_list_identity, "Population":population_list_population})
#attributes_df=pd.DataFrame({"Parent" :  attributes_list_parent, "Identity" : attributes_list_identity})
#muller(populations_df, attributes_df, color_by = "Identity")

attributes_file.close()
population_file.close()
population_table_file.close()