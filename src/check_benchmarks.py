#!/usr/bin/env python3
# Usage: check_benchmarks.py <path_to_benchmarks_directory>
# The benchmarks directory is expected to contain
# one sub-directory per benchmarked rule, and those
# subdirectories should contain benchmarks outputs for
# the corresponding rule instances.

import sys
from pathlib import Path
import pandas as pd


bench_dir = Path(sys.argv[1])
bench_dirs = {
    dir_path.name: dir_path
    for dir_path in bench_dir.iterdir()}
bench_tables = {
    bench_name: pd.concat([
        pd.read_table(table_path)
        for table_path
        in bench_dirs[bench_name].iterdir()])
    for bench_name in bench_dirs}

all_bench = pd.concat(bench_tables)

max_ressources = pd.concat(
    [
        all_bench.groupby(level=0)[colname].max()
        for colname
        in all_bench.drop(columns=["h:m:s"]).columns],
    axis=1)

median_ressources = pd.concat(
    [
        all_bench.groupby(level=0)[colname].median()
        for colname
        in all_bench.drop(columns=["h:m:s"]).columns],
    axis=1)

print(max_ressources.to_string())
print(median_ressources.to_string())
