#!/bin/bash

# Usage: QOS=<slurm qos> PART=<slurm partition> ./run_workflow.sh [<snakemake_config> [<other snakemake options>]]

if [[ ${VENV} ]]
then
    source ${VENV}/bin/activate
fi

SCRIPT_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
WORKFLOW="jointcall"
profile="${SCRIPT_DIR}/profile/slurm"
[ -d ${profile} ] || error_exit "Missing profile directory: ${profile}"
workflow_profile="${SCRIPT_DIR}/workflow/profile"
[ -d ${workflow_profile} ] || error_exit "Missing workflow profile directory: ${workflow_profile}"

# http://linuxcommand.org/wss0150.php
PROGNAME=$(basename $0)

function error_exit
{
#   ----------------------------------------------------------------
#   Function for exit due to fatal program error
#       Accepts 1 argument:
#           string containing descriptive error message
#   ----------------------------------------------------------------
    echo "${PROGNAME}: ${1:-"Unknown Error in ${WORKFLOW}"}" 1>&2
    exit 1
}

function print_eval
{
    cmd="$@"
    >&2 echo ${cmd}
    eval ${cmd} || error_exit "${cmd} failed"
}


[ ${QOS} ] || QOS="hubbioit"
[ ${PART} ] || PART="hubbioit"


# TODO: Problem if we want to have options to pass to snakemake but no config file
if [ "${1}" ]
then
    snakemake_config=${1}
    config_option="--configfile ${snakemake_config}"
    shift
else
    config_option=""
fi

snakefile="${SCRIPT_DIR}/workflow/${WORKFLOW}.snakefile"
[ -e ${snakefile} ] || error_exit "Missing snakefile: ${snakefile}"
echo "Snakemake version:"
echo $(which snakemake)
snakemake --version

cmd="mkdir -p logs/cluster/${WORKFLOW}"
print_eval ${cmd}

cmd="snakemake -s ${snakefile} ${config_option} \
    --executor slurm
    --profile ${profile} \
    --workflow-profile ${workflow_profile} \
    --software-deployment-method apptainer \
    $@"

module load apptainer/1.3.1
export APPTAINER_BINDPATH="/pasteur:/pasteur"
# --mem=10G to avoid a bug
# See https://github.com/snakemake/snakemake/issues/2230#issuecomment-1521386382
>&2 sbatch --qos="${QOS}" --partition="${PART}" --parsable \
    -J run_${WORKFLOW} \
    --mem=10G \
    -o logs/cluster/${WORKFLOW}/${WORKFLOW}.o \
    -e logs/cluster/${WORKFLOW}/${WORKFLOW}.e \
        ${cmd} \
    || error_exit "sbatch snakemake failed for ${WORKFLOW}"

exit 0
