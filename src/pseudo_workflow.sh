module load fastqc/0.11.9
module load cutadapt/2.10
module load bwa/0.7.17
module load graalvm R/3.6.2 picard-tools/2.23.3
module load java/1.8.0 GenomeAnalysisTK/4.1.9.0

#quality step
fastqc -t "thread_nb" R1.fastq.gz R2.fastq.gz -o "output_dir"

#trimming step
cutadapt -j "thread_nb" --trim-n -q 30 --minimum-length=20 -o R1_clean.fastq.gz -p R2_clean.fastq.gz R1.fastq.gz R2.fastq.gz

#quality step
fastqc -t "thread_nb" R1_clean.fastq.gz R2_clean.fastq.gz -o "output_dir"

#mapping step
bwa index -p "prefix" "ref.fasta"
bwa mem -t "thread_nb" -M -R "voir_avec_pascal" "path_to_index" R1.fastq.gz R2.fastq.gz

#deduplication step
picard MarkDuplicates -I "bam" -O "dedup.bam" --REMOVE_DUPLICATES true -M dedup_metrics.txt

#realignment step
GenomeAnalysisTK -T RealignerTargetCreator -R "ref.fasta" -I "dedup.bam" -o "bam.intervals" -nt "thread_nb"
GenomeAnalysisTK -T IndelRealigner -targetIntervals "bam.intervals" -R "ref.fasta" -I "dedup.bam" -o "dedup_realigned.bam"

#base quality recalibration step (not mandatory...)
#GenomeAnalysisTK BaseRecalibrator -I "dedup_realigned.bam" -R "ref.fasta" -O "recal_data.table"

