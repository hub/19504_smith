General biological question: Membrane evolution in bacteria

Meeting 18/01/2024
==================

3 souches, avec ~20 de points de temps représentant 500 générations.

Damien: voit avec Robert pour les données et le génome de ref

Petit génome, ref dispo, reset à T0 ? Éliminer les polymorphismes en fonction de T0 a posteriori.

Blaise: Peut partir d'un workflow existant de joint SNP calling pour données populationnelles, avec freeBayes et snpEFF

Pascal/Damien: pseudo-workflow des étapes clé
-> TODO Blaise: Montrer à Damien les étapes existante dans `jointcall_from_gip`

Utiliser les biais mesurés globalement pour faire du filtrage après le SNP calling.

Filtrer sur le total de comptage, ou minimum 1% chez au moins un échantillon. Seuils conseillés par Garrison (auteur FreeBayes)

indels: bbmap ? Sinon, Freebayes fait les petits événements.

Pierre:
- Fitness landscape ?
- Localisation des mutations sur le génome
    - Extrait depuis .vcf

Fischer/Mueller plot: il y a des libraries python ou R qui le font

Pascal:
Régression logistique, linéariser la pente, -> coeff de sélection, regrouper les événements d'adaptation, par type de trajectoire.


Meeting 23/01/2024
==================


Graph: filter mutations for clarity (based on min freq, based on effect)

Only a few mutations are fixed at the end.

TODO Robert: ask for project space.

Deadline: the sooner the better

We can start with a small test sample

Meeting (26/03/2024)
====================

TODO: freq heatmap for end points:
Rows:

SK-WT
SK-Delta

NS-WT
NS-Delta

Osmo-WT
Osmo-Delta


Columns: effects
Colour: Max frequency or sum frequency for SNPs having the same effect
-> Or keep individual loci, grouped by gene and effect

Filters:

Keep only loci where a minimal frequency has been reached for at least one of the samples
Exclude "Evo" samples
exclude non-synonymous
Only inside genes, unless very high frequency (0.5)
(The ref genome is very close to WT)

Try first with just SNPs sorted by genomic coordinates, (still label by `gene_id`)


End of May: Robert leaves Pasteur. Aim: submit paper end of April
17th April: Department meeting


Meeting (29/04/2024)
====================

TODO Pascal: Send filtering script.
TODO Blaise: Include this filter as extra step in the workflow.
TODO Robert: Send paper draft

TODO All: write methods
